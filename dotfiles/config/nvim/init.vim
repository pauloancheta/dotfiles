call plug#begin('~/.nvim/plugged')
  Plug 'airblade/vim-gitgutter'
  Plug 'kien/ctrlp.vim'
  Plug 'rking/ag.vim'
  Plug 'scrooloose/syntastic'
  Plug 'scrooloose/nerdtree'
  Plug 'terryma/vim-multiple-cursors'
  Plug 'tpope/vim-fugitive'
  Plug 'tpope/vim-sensible'
  Plug 'tpope/vim-rails'
  Plug 'vim-scripts/HTML-AutoCloseTag'
  Plug 'sheerun/vim-polyglot' " SYNTAX HIGHLIGHTS
  Plug 'jiangmiao/auto-pairs'
  Plug 'gko/vim-coloresque'

  " Plug 'janko-m/vim-test'
  " Plug 'bling/vim-airline'
  " Plug 'godlygeek/tabular'
  " Plug 'garbas/vim-snipmate'
  " Plug 'tomtom/tlib_vim' " snipmate dep
  " Plug 'MarcWeber/vim-addon-mw-utils' " snipmate dep
  " Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' } " Plugin outside ~/.vim/plugged with post-update hook
  " Plug 'https://github.com/junegunn/vim-github-dashboard.git' " Using git URL
call plug#end()

" SYNTASTIC
set statusline=%f:%l\ %m%=[line\ %l\/%L]
let g:syntastic_enable_signs=1
let g:syntastic_auto_loc_list=2

" VIM-MULTIPLE-CURSORS
let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_next_key='<C-n>'
let g:multi_cursor_quit_key='<Esc>'

" GIT-GUTTER
let g:gitgutter_realtime = 0
let g:gitgutter_eager = 0

" NERDtree
let g:NERDTreeChDirMode       = 2
let g:ctrlp_working_path_mode = 'rw'

" AUTO-PAIRS
let g:AutoPairsFlyMode = 0

" COLORS
set colorcolumn=100
set background=dark
let g:solarized_termcolors=32
let g:solarized_termtrans=1
colorscheme solarized
highlight ColorColumn guibg=Black

" ------------------------------------------------------------------------------------
"                                  MORE CONFIG!
" ------------------------------------------------------------------------------------
set tabstop=2     " Size of a hard tabstop
set shiftwidth=2  " Size of an 'indent'
                  " a combination of spaces and tabs are used to simulate tab stops at a width
                  " other than the (hard)tabstop
set softtabstop=2
set smarttab      " Make 'tab' insert indents instead of tabs at the beginning of a line
set expandtab     " always use spaces instead of tab characters
set splitright    " v split
set number        " line numbers
set autoread      " auto refresh
set hid           " a buffer becomes hidden when it is abandoned
set noswapfile    " take out swap files
set cursorline!   " see where the cursor is easier
set nopaste       " get that annoying paste out
set nohls         " don't highlight everything
set so=10         " have cursor relatively in the middle
set nopaste

" ------------------------------------------------------------------------------------
"                                  KEY BINDINGS
" ------------------------------------------------------------------------------------
let mapleader=" "

" SPLITS
nmap <silent> <leader>W :split<CR>
nmap <silent> <leader>w :vsplit<CR>

" WINDOW RESIZE
nnoremap <A-H> :vertical resize +10<CR>
nnoremap <A-L> :vertical resize -10<CR>
nnoremap <A-K> :resize -5<CR>
nnoremap <A-J> :resize +5<CR>

" TABS
nmap <C-T> :tabe<CR>
" map <leader>h :tabn<CR>
" map <leader>l :tabp<CR>

" COPY AND PASTE LINES
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" YANK TO CLIPBOARD
imap <C-V> <Esc>"+gPi
vmap <C-C> "+y

imap qq <ESC>

" TERMINAL
:tnoremap <Esc> <C-\><C-n>

" NERDTree
nnoremap <silent> <leader>\ :NERDTreeToggle<CR>

" ------------------------------------------------------------------------------------
"                                  FUNCTIONS
" ------------------------------------------------------------------------------------
" Show trailing whitespace only after some text (ignores blank lines):
hi ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

" Auto trim white space
fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun
autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()
