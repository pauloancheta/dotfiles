model_spec_files = Dir['spec/models/**/*']
model_files = Dir['app/models/**/*']

model_files.map! do |f|
  f = f.split('/').last
  f = f.gsub!('.rb', '')
end

model_spec_files.map! do |f|
  f = f.split('/').last
  f = f.gsub!('_spec.rb', '')
end

output = model_files - model_spec_files

output.each do |o|
  puts "- [ ] #{o}.rb"
end
